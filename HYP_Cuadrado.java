package herenciaypoliformismo;

public class HYP_Cuadrado extends HYP_Figura {
	private float lado;

	public HYP_Cuadrado() {
		super();
	}

	public HYP_Cuadrado(String pNombre, float pLado) {
		super(pNombre);
		lado=pLado;
	}
	
	public float getLado() {
		return lado;
	}

	public void setLado(float lado) {
		this.lado = lado;
	}

	
	@Override
	public float calcularPerimetro() {

		return lado*4;
	}

	@Override
	public float calcularSuperficie() {
		return lado*lado;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + Float.floatToIntBits(lado);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (!(obj instanceof HYP_Cuadrado)) {
			return false;
		}
		HYP_Cuadrado other = (HYP_Cuadrado) obj;
		if (Float.floatToIntBits(lado) != Float.floatToIntBits(other.lado)) {
			return false;
		}
		
		return true;
	}

	@Override
	public String toString() {
		return super.toString() + "\nlado=" + lado;
	}
	
	
}
