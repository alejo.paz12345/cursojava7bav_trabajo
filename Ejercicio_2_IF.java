package ejerciciosIfyCiclos;

import java.awt.Color;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.AbstractAction;
import java.awt.event.ActionEvent;
import javax.swing.Action;

public class Ejercicio_2_IF {

	private JFrame frame;
	private JTextField textField;
	private JTextField textField_1;

	/**
	 * Launch the application.
	 */
	Double Numero;
	String CONDICION ;
	private final Action action = new SwingAction();
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ejercicio_2_IF window = new Ejercicio_2_IF();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Ejercicio_2_IF() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setBackground(Color.WHITE);
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Ingresar el numero deseado");
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setBounds(0, 0, 434, 20);
		frame.getContentPane().add(lblNewLabel);
		
		JLabel lblNumeroIngresado = new JLabel("Numero ingresado:");
		lblNumeroIngresado.setBounds(10, 70, 120, 14);
		frame.getContentPane().add(lblNumeroIngresado);
		
		textField = new JTextField();
		textField.setBounds(135, 67, 86, 20);
		frame.getContentPane().add(textField);
		textField.setColumns(10);
		
		JLabel lblParOImpar = new JLabel("Par o Impar :");
		lblParOImpar.setForeground(Color.BLACK);
		lblParOImpar.setBackground(Color.BLACK);
		lblParOImpar.setBounds(30, 144, 100, 14);
		frame.getContentPane().add(lblParOImpar);
		
		textField_1 = new JTextField();
		textField_1.setForeground(Color.WHITE);
		textField_1.setBackground(Color.BLACK);
		textField_1.setEditable(false);
		textField_1.setBounds(135, 141, 86, 20);
		frame.getContentPane().add(textField_1);
		textField_1.setColumns(10);
		
		JButton btnNewButton = new JButton("Averiguar");
		btnNewButton.setAction(action);
		btnNewButton.setBounds(300, 95, 89, 23);
		frame.getContentPane().add(btnNewButton);
	}
	@SuppressWarnings("serial")
	private class SwingAction extends AbstractAction {
		public SwingAction() {
			putValue(NAME, "Averiguar");
			putValue(SHORT_DESCRIPTION, "Some short description");
		}
		public void actionPerformed(ActionEvent e) {
			
			Numero=  Double.parseDouble(textField.getText());
			
			if (Numero % 2 == 0 ) {
				CONDICION = "PAR";
				textField_1.setForeground(Color.GREEN);
			}
			else{
				CONDICION = "IMPAR";
				textField_1.setForeground(Color.CYAN);
			}
			textField_1.setText(String.valueOf(CONDICION));
			
			
		}
	}
}
