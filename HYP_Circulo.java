package herenciaypoliformismo;


public class HYP_Circulo extends HYP_Figura {
	private float radio;

	public HYP_Circulo() {
		// TODO Auto-generated constructor stub
	}

	public HYP_Circulo(String pNombre, float pRadio) {
		super(pNombre);
		radio=pRadio;
	}
	
	
	public float getRadio() {
		return radio;
	}

	public void setRadio(float radio) {
		this.radio = radio;
	}

	@Override
	public float calcularPerimetro() {
	return (float) Math.PI*2*radio;
	}

	@Override
	public float calcularSuperficie() {
		return (float)Math.PI*radio*radio;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + Float.floatToIntBits(radio);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (!(obj instanceof HYP_Circulo)) {
			return false;
		}
		HYP_Circulo other = (HYP_Circulo) obj;
		if (Float.floatToIntBits(radio) != Float.floatToIntBits(other.radio)) {
			return false;
		}
		
		return true;
	}

	@Override
	public String toString() {
		return super.toString() + "\nradio=" + radio;
	}
	
	

	
}

