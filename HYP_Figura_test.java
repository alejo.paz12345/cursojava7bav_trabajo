package herenciaypoliformismo;


import java.util.ArrayList;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import herenciaypoliformismo.HYP_Circulo;
import herenciaypoliformismo.HYP_Cuadrado;



public class HYP_Figura_test<Figura> {
	List<Figura>lstFiguras= new ArrayList<Figura>();
	Set<Figura> setFiguras= new HashSet<Figura>();
	
	HYP_Cuadrado cuadrado;
	HYP_Circulo circulo;

	@SuppressWarnings("unchecked")
	@Before
	public void setUp() throws Exception {
	cuadrado=new HYP_Cuadrado("cuadradoTest",10);
	circulo= new HYP_Circulo("circuloTest",10);
	lstFiguras.add((Figura) cuadrado);
	lstFiguras.add((Figura) circulo);
	lstFiguras.add((Figura) new HYP_Cuadrado("cuadrado2",20));
	lstFiguras.add((Figura) new HYP_Circulo("circulo2",20));
	
	setFiguras.add((Figura) cuadrado);
	setFiguras.add((Figura) circulo);
	setFiguras.add((Figura) new HYP_Cuadrado("cuadrado2",20));
	setFiguras.add((Figura) new HYP_Circulo("circulo2",20));

	}

	@After
	public void tearDown() throws Exception {
		cuadrado=null;
		circulo=null;
		lstFiguras=null;
		setFiguras=null;
	}

	@Test
	public void testGetNombreDelCuadradoDelConstructor() {
		Assert.assertEquals("cuadradoTest", cuadrado.getNombre());
		
	}
	@Test
	public void testGetDelLadoDelCuadrado() {
	 Assert.assertEquals(10.0f, cuadrado.getLado(),0.1);
	}
	
	
	

	@Test
	public void testCalcularPerimetroDelCuadrado() {
	Assert.assertEquals(40.0f, cuadrado.calcularPerimetro(),0.1);
	}

	@Test
	public void testCalcularSuperficieDelCuadrado() {
		Assert.assertEquals(100.0f, cuadrado.calcularSuperficie(), 0.1);
	}
	
    @Test
    public void testCalcularPerimetroDelCirculo() {
    	Assert.assertEquals(62.83, circulo.calcularPerimetro(),0.01);
    	
    }
    
    @Test
    public void testContieneLaListaUnCuadrado() {
    	Assert.assertTrue(lstFiguras.contains(cuadrado));
    }
    @Test
    public void testNoContieneLaListaUnCuadrado() {
    	cuadrado.setNombre("Otro cuadrado");
    	Assert.assertFalse(lstFiguras.contains(new HYP_Cuadrado("otro Cuadrado",10)));
    }
    @Test
    public void testContieneLaListaUnCirculo() {
    	Assert.assertTrue(lstFiguras.contains(circulo));
    }
    @Test
    public void testNoContieneLaListaUnCirculo() {
    	circulo.setNombre("Otro circulo");
    	Assert.assertFalse(lstFiguras.contains(new HYP_Circulo("otro Circulo",10)));
    }
    
}
