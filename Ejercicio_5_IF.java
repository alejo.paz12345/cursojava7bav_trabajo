package ejerciciosIfyCiclos;

import java.awt.EventQueue;
import javax.swing.JFrame;
import java.awt.Color;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.AbstractAction;
import java.awt.event.ActionEvent;
import javax.swing.Action;
import java.awt.event.ActionListener;
import java.awt.SystemColor;
import java.awt.Font;

public class Ejercicio_5_IF {

	private JFrame frame;
	private JTextField textField;
	private final Action action = new SwingAction();

	/**
	 * Launch the application.
	 */
	String pos, pos1;
	private JTextField textField_1;
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ejercicio_5_IF window = new Ejercicio_5_IF();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Ejercicio_5_IF() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setBackground(Color.WHITE);
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Por favor, ingrese la posicion en la que termino la competencia:");
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setBounds(0, 0, 434, 20);
		frame.getContentPane().add(lblNewLabel);
		
		JLabel lblPosicion = new JLabel("Posicion:");
		lblPosicion.setBounds(69, 76, 60, 14);
		frame.getContentPane().add(lblPosicion);
		
		textField = new JTextField();
		textField.setBounds(251, 73, 123, 20);
		frame.getContentPane().add(textField);
		textField.setColumns(10);
		
		JButton btnNewButton = new JButton("Averiguar");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int pos1;
				pos=(textField.getText());
				pos1=Integer.parseInt(textField.getText());
				
			if(pos.equals("1") || pos.equals("primero") || pos.equals("Primero")|| pos.equals("PRIMERO")) {
	
				textField_1.setText(String.valueOf("Felicitaciones obtuviste la medalla de oro"));
				
				}
				if(pos.equals("2") || pos.equals("segundo") || pos.equals("Segundo")|| pos.equals("SEGUNDO")) {
					textField_1.setText(String.valueOf("Felicitaciones obtuviste la medalla de plata"));
				
				}
				if(pos.equals("3") || pos.equals("tercero") || pos.equals("Tercero")|| pos.equals("TERCERO")) {
					textField_1.setText(String.valueOf("Felicitaciones obtuviste la medalla de bronce"));
					
				}
				if(pos1 >=4 ) {
					textField_1.setText(String.valueOf("Segui participando"));
					textField_1.setForeground(Color.RED);
				}
			}
		});
		btnNewButton.setAction(action);
		btnNewButton.setBounds(170, 198, 89, 23);
		frame.getContentPane().add(btnNewButton);
		
		textField_1 = new JTextField();
		textField_1.setFont(new Font("Tahoma", Font.PLAIN, 11));
		textField_1.setBackground(SystemColor.menu);
		textField_1.setBounds(69, 129, 305, 20);
		frame.getContentPane().add(textField_1);
		textField_1.setColumns(10);
	}

	@SuppressWarnings("serial")
	private class SwingAction extends AbstractAction {
		public SwingAction() {
			putValue(NAME, "Averiguar");
			putValue(SHORT_DESCRIPTION, "Some short description");
		}
		public void actionPerformed(ActionEvent e) {
		}
	}
}
