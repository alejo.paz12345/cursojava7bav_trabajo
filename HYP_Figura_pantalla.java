package herenciaypoliformismo;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JTable;
import javax.swing.JScrollPane;
import javax.swing.table.DefaultTableModel;
import javax.swing.JButton;

public class HYP_Figura_pantalla {

	private JFrame frame;
	private JTextField textNombre;
	private JTextField textValor;
	private JTable tableFiguras;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					HYP_Figura_pantalla window = new HYP_Figura_pantalla();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public HYP_Figura_pantalla() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 449, 545);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
				
		JLabel lblNewLabel = new JLabel("Figuras geometricas");
		lblNewLabel.setFont(new Font("Arial Black", Font.PLAIN, 30));
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setBounds(28, 29, 377, 47);
		frame.getContentPane().add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("Nombre:");
		lblNewLabel_1.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel_1.setFont(new Font("Arial Black", Font.BOLD | Font.ITALIC, 15));
		lblNewLabel_1.setBounds(21, 121, 92, 22);
		frame.getContentPane().add(lblNewLabel_1);
		
		JLabel lblValor = new JLabel("Valor:");
		lblValor.setHorizontalAlignment(SwingConstants.CENTER);
		lblValor.setFont(new Font("Arial Black", Font.BOLD | Font.ITALIC, 15));
		lblValor.setBounds(21, 167, 92, 22);
		frame.getContentPane().add(lblValor);
		
		textNombre = new JTextField();
		textNombre.setBounds(146, 124, 245, 20);
		frame.getContentPane().add(textNombre);
		textNombre.setColumns(10);
		
		textValor = new JTextField();
		textValor.setBounds(146, 170, 245, 20);
		frame.getContentPane().add(textValor);
		textValor.setColumns(10);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(41, 239, 350, 100);
		frame.getContentPane().add(scrollPane);
		
		tableFiguras = new JTable();
		tableFiguras.setModel(new DefaultTableModel(
			new Object[][] {
				{"Cuadrado", "10", "10"},
				{"Circulo", "10", "20"},
			},
			new String[] {
				"Nombre", "Perimetro", "Superficie"
			}
		));
		scrollPane.setViewportView(tableFiguras);
		
		JButton btnNewButton = new JButton("Crear Cuadrado");
		btnNewButton.setBounds(28, 362, 377, 23);
		frame.getContentPane().add(btnNewButton);
		
		JButton btnCrearCirculo = new JButton("Crear Circulo");
		btnCrearCirculo.setBounds(28, 403, 377, 23);
		frame.getContentPane().add(btnCrearCirculo);
	}
}
