package ejerciciosIfyCiclos;



import java.awt.Color;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.AbstractAction;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

public class Ejercicio_1_IF{

	private JFrame frame;
	private JTextField txtNotaN�1;
	private JTextField txtNotaN�2;
	private JTextField txtNotaN�3;
	
	
	

	/**
	 * Launch the application.
	 */
    double Nota1, Nota2, Nota3, Promedio;
    String CONDICION;
	private JTextField textField;
	private JTextField textField_1;
	private final javax.swing.Action action_1 = new SwingAction_1();
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ejercicio_1_IF window = new Ejercicio_1_IF();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Ejercicio_1_IF() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setBackground(Color.WHITE);
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Indicar las notas del Alumno");
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setBounds(0, 0, 434, 20);
		frame.getContentPane().add(lblNewLabel);
		
		JLabel lblNotan�1 = new JLabel("Nota 1:");
		lblNotan�1.setBounds(30, 61, 46, 14);
		frame.getContentPane().add(lblNotan�1);
		
		JLabel lblNotan�2 = new JLabel("Nota 2:");
		lblNotan�2.setBounds(30, 100, 46, 14);
		frame.getContentPane().add(lblNotan�2);
		
		JLabel lblNotan�3 = new JLabel("Nota 3:");
		lblNotan�3.setBounds(30, 139, 46, 14);
		frame.getContentPane().add(lblNotan�3);
		
		txtNotaN�1 = new JTextField();
		txtNotaN�1.setBounds(86, 58, 86, 20);
		frame.getContentPane().add(txtNotaN�1);
		txtNotaN�1.setColumns(10);
		
		txtNotaN�2 = new JTextField();
		txtNotaN�2.setBounds(86, 100, 86, 20);
		frame.getContentPane().add(txtNotaN�2);
		txtNotaN�2.setColumns(10);
		
		txtNotaN�3 = new JTextField();
		txtNotaN�3.setBounds(86, 136, 86, 20);
		frame.getContentPane().add(txtNotaN�3);
		txtNotaN�3.setColumns(10);
		
		JButton btnNewButton = new JButton("Calcular");
		btnNewButton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0) {
				
				Nota1 =  Double.parseDouble(txtNotaN�1.getText());
				Nota2 =  Double.parseDouble(txtNotaN�2.getText());
				Nota3 =  Double.parseDouble(txtNotaN�3.getText());
				
			Promedio = Nota1 + Nota2 + Nota3;
			Promedio = Promedio/3;
			
			if (Promedio < 7) {
				CONDICION = "DESAPROBADO";
				textField_1.setForeground(Color.RED);
			}
			else{
				CONDICION = "APROBADO";
				textField_1.setForeground(Color.GREEN);
			}
			
			textField.setText(String.valueOf(Promedio));
			
			textField_1.setText(String.valueOf(CONDICION));
			}
		});
		btnNewButton.setAction(action_1);
		btnNewButton.setBounds(297, 96, 89, 23);
		frame.getContentPane().add(btnNewButton);
		
		JLabel lblpromedio = new JLabel("Promedio:");
		lblpromedio.setBounds(94, 204, 120, 14);
		frame.getContentPane().add(lblpromedio);
		
		textField = new JTextField();
		textField.setBackground(Color.LIGHT_GRAY);
		textField.setEditable(false);
		textField.setBounds(235, 201, 120, 20);
		frame.getContentPane().add(textField);
		textField.setColumns(10);
		
		textField_1 = new JTextField();
		textField_1.setBackground(Color.LIGHT_GRAY);
		textField_1.setEditable(false);
		textField_1.setBounds(235, 232, 120, 20);
		frame.getContentPane().add(textField_1);
		textField_1.setColumns(10);
		
		JLabel lblCondicionDelAlumno = new JLabel("Condicion del Alumno:");
		lblCondicionDelAlumno.setBounds(62, 235, 152, 14);
		frame.getContentPane().add(lblCondicionDelAlumno);
	}
	
	@SuppressWarnings("serial")
	private class SwingAction_1 extends AbstractAction {
		public SwingAction_1() {
			putValue(NAME, "Calcular");
			putValue(SHORT_DESCRIPTION, "Some short description");
		}
		public void actionPerformed(ActionEvent e) {
		}
	}
}
