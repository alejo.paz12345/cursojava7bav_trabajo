package herenciaypoliformismo;

public abstract class HYP_Figura {

	private String nombre;

	public HYP_Figura() {
		nombre = "Sin nombre";
	}

	public HYP_Figura(String pNombre) {
		// TODO Auto-generated constructor stub
		super();
		this.nombre = pNombre;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String pNombre) {
		this.nombre = pNombre;
	}
	
	
	
	public abstract float calcularPerimetro();
	
	
	public abstract float calcularSuperficie();

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((nombre == null) ? 0 : nombre.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof HYP_Figura)) {
			return false;
		}
		HYP_Figura other = (HYP_Figura) obj;
		if (nombre == null) {
			if (other.getNombre() != null) {
				return false;
			}
		} else if (!nombre.equals(other.getNombre())) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "\nnombre=" + nombre;
	}

}


